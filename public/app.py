import os

from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/search', methods=['POST'])
def search():
    # Lấy nội dung cần tìm kiếm từ form
    query = request.form['query']

    # Tìm kiếm các tệp chứa nội dung từ \begin{ex} đến \end{ex} trong các thư mục
    results = []
    for root, dirs, files in os.walk('public/mytex'):
        for file in files:
            if file.endswith('.tex'):
                with open(os.path.join(root, file)) as f:
                    content = f.read()
                    matches = re.findall(r'\\begin\{ex\}.*?\\end\{ex\}', content, re.DOTALL)
                    for match in matches:
                        if query in match:
                            results.append(match)

    # Hiển thị kết quả tìm kiếm trên trang web
    return render_template('search.html', query=query, results=results)
# @app.route('/')
# def index():
#     return render_template('index.html')

# @app.route('/search', methods=['POST'])
# def search():
#     # Lấy nội dung cần tìm kiếm từ form
#     query = request.form['query']

#     # Tìm kiếm các tệp chứa nội dung từ \begin{ex} đến \end{ex} trong các thư mục
#     results = []
#     for root, dirs, files in os.walk('public/mytex'):
#         for file in files:
#             if file.endswith('.tex'):
#                 with open(os.path.join(root, file)) as f:
#                     content = f.read()
#                     matches = re.findall(r'\\begin\{ex\}.*?\\end\{ex\}', content, re.DOTALL)
#                     for match in matches:
#                         if query in match:
#                             results.append(match)

#     # Hiển thị kết quả tìm kiếm trên trang web
#     return render_template('search.html', query=query, results=results)
