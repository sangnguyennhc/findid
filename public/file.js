function texfind() {
  const searchText = document.querySelector('#texsearch').value;
  const xhr = new XMLHttpRequest();
  xhr.open('GET', `/search?query=${searchText}`);
  xhr.onload = function() {
    const results = document.querySelector('#results');
    results.innerHTML = xhr.responseText;
  };
  xhr.send();
}
