function showImage(url) {
  const imgElement = document.createElement('img');
  imgElement.src = url;
  document.querySelector('#images').appendChild(imgElement);
}
