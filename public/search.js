const searchInput = document.querySelector("#search");
const imagesDiv = document.querySelector("#images");
const searchBtn = document.querySelector("#search-btn");

function searchImages() {
  const searchTerm = searchInput.value.toLowerCase();

  fetchImages(searchTerm).then(function (images) {
    imagesDiv.innerHTML = "";

    images.forEach(function (image) {
      const imgElement = document.createElement("img");
      imgElement.src = image.url;
      imgElement.alt = image.name;
      imagesDiv.appendChild(imgElement);
    });
  });
}

searchBtn.addEventListener("click", searchImages);
searchInput.addEventListener("keydown", function (event) {
  if (event.keyCode === 13) {
    searchImages();
  }
});

async function fetchImages(searchTerm) {
  const images = [];

  const response = await fetch("./images.json");
  const data = await response.json();

  data.forEach(function (directory) {
    directory.files.forEach(function (file) {
      if (file.name.toLowerCase().includes(searchTerm)) {
        const url = `${directory.name}/${file.name}`;
        images.push({
          name: file.name,
          url: url,
        });
      }
    });
  });

  return images;
}
