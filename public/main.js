function search(event) {
  event.preventDefault();

  const searchQuery = document.getElementById('search-input').value;
  const searchType = document.getElementById('type').value;

  const searchUrl = `https://gitlab.com/api/v4/projects/44525121/search?scope=blobs&search=${searchQuery}&ref=master&file_name=*.${searchType}`;

  fetch(searchUrl)
    .then(response => response.json())
    .then(data => {
      const searchResults = document.getElementById('search-results');
      searchResults.innerHTML = '';

      if (data.length === 0) {
        searchResults.innerHTML = 'Không tìm thấy kết quả nào.';
      } else {
        data.forEach(result => {
          const resultUrl = `https://gitlab.com/sangnguyennhc/findid/-/blob/master/${result.path}#L${result.line}`;
          const resultItem = `
            <div>
              <a href="${resultUrl}" target="_blank">${result.path} (${result.line})</a>
              <p>${result.context}</p>
            </div>
          `;
          searchResults.insertAdjacentHTML('beforeend', resultItem);
        });
      }
    })
    .catch(error => {
      console.error('Lỗi khi tìm kiếm:', error);
    });
}
